#!/bin/sh
echo "Notifying..."
curl --request GET \
  --url http://lnkbnk.atommac.com:9000/hooks/deploy \
  --header "X-Lnkbnk-Deploy-Auth: $WEBHOOK_DEPLOY_KEY"

curl "http://gotify.atommac.com/message?token=${GOTIFY_API_KEY}" \
  -F "title=LNKBNK" \
  -F "message=Pipeline Finished!" \
  -F "priority=5"
