import { defineConfig } from "cypress";

export default defineConfig({
  e2e: {
    baseUrl: "http://localhost:3000",
    specPattern: "cypress/e2e/**/*.cy.{js,jsx,ts,tsx}",
    defaultCommandTimeout: 90000,
    execTimeout: 90000,
    taskTimeout: 90000,
    pageLoadTimeout: 90000,
    requestTimeout: 90000,
    responseTimeout: 90000,
  },
});
