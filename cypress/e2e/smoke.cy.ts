import faker from "@faker-js/faker";

describe("smoke tests", () => {
  beforeEach(() => {
    // Some tests rely on there _not_ being a logged in user.  Some helperThis
    // commands that rely on the value of the `@user` alias to always be present
    // even if it is empty. This line prevents those methods from throwing
    // errors while tests run.  For example, the cleanupUser command.
    cy.wrap({}).as("user");
  });
  afterEach(() => {
    cy.cleanupUser();
  });
  it("should redirect to login if unauthenticated", () => {
    cy.visit("/admin");
    cy.url().should("include", "/login?redirectTo=%2Fadmin");
  });
  it("should be able to log in", () => {
    const loginForm = {
      email: `${faker.internet.userName()}@example.com`,
      password: "myreallystrongpassword",
    };

    cy.createUser(loginForm);
    cy.visit("/login");
    cy.findByLabelText("Email address").type(loginForm.email);
    cy.findByLabelText("Password").type(loginForm.password);
    cy.contains("Log in").click();
    cy.url().should("include", "/admin/files");
  });
  it("should be able to logout", () => {
    cy.login();
    cy.visit("/admin");
    cy.findAllByText(/logout/i)
      .first()
      .click();
    cy.url().should("include", "/");
  });
  it("should be able to create and view a share", () => {
    // Navigator is not defined in E2E tests for some reason...
    cy.window().then((win) => {
      Object.defineProperty(win, "navigator", {
        writable: true,
        enumerable: true,
        value: {
          clipboard: {},
        },
      });
    });

    cy.login();
    cy.visit("/admin/files");

    cy.intercept("POST", "/admin/shares/create*").as("create-endpoint");

    // Get menu button for first item in the file list and click it.
    cy.get('table [role="menubar"]').first().click();

    // Context menu should now be visible.
    cy.get('table [role="menu"]')
      .first()
      .children()
      .last()
      .should("be.visible");

    // When the context menu opens, click the "Share (x)" button.
    cy.get('table [role="menubar"] form').first().submit();

    // Make sure a new share was created.
    cy.wait("@create-endpoint");
    cy.get("@create-endpoint").its("response.statusCode").should("equal", 200);

    // View the share
    cy.intercept("GET", "/shares/*").as("share-view");

    cy.window().then(async (win) => {
      const url = await win.navigator.clipboard.readText();
      cy.visit(url);
    });
  });
  it("should be able to delete a share", () => {
    cy.login();
    cy.intercept("POST", "/admin/shares/delete*").as("delete-endpoint");
    cy.visit("admin/shares/all");
    cy.get("tbody a").first().click();
    cy.get('table [role="menubar"]').first().click();
    cy.get('table [role="menubar"] form').first().submit();
    // Make sure a new share was deleted.
    cy.wait("@delete-endpoint");
    cy.get("@delete-endpoint").its("response.statusCode").should("equal", 200);
  });
});
