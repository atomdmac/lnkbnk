#!/bin/sh
# Provisions a host running Debian with the necessary software to run LNKBNK
# in production.  This involves copying config files to the appropriate place
# starting supporting services, etc.

set -e

APP_ROOT=/app/lnkbnk
WEBHOOK_CONFIG_DEST=/etc/webhook
WEBHOOK_SERVICE_DEST=/etc/systemd/system
SERVICES_CONFIG_DEST=/etc/sysconfig

# Go to working dir
cd "$APP_ROOT"

# Include production secrets
. ./.env

# Download Go
apt update && apt install webhook

# Set up webhook service
if [ ! -e $WEBHOOK_CONFIG_DEST ]; then
  echo "Copying webhook.json to $WEBHOOK_CONFIG_DEST"
  mkdir -p /etc/webhook
  ln -s "$APP_ROOT/scripts/ci/provision/webhook.json" "$WEBHOOK_CONFIG_DEST/webhook.json"
else
  echo "$WEBHOOK_CONFIG_DEST/webhook.json already exists.  Skipping..."
fi

# Set up configs for supporting services
mkdir -p "$SERVICES_CONFIG_DEST"

# Load environment variables that should be passed to systemd service units.
if [ ! -e "$SERVICES_CONFIG_DEST/lnkbnk.env" ]; then
  echo "Linking webhook config file to $SERVICES_CONFIG_DEST/lnkbnk.env"
  ln -s "$APP_ROOT/.env" "$SERVICES_CONFIG_DEST/lnkbnk.env"
fi

# Set up webhook service used to listen for deployable events.
if [ ! -e "$WEBHOOK_SERVICE_DEST/webhook.service" ]; then
  echo "Creating symlink at $WEBHOOK_SERVICE_DEST/webhook.service"
  ln -s "$APP_ROOT/scripts/ci/provision/webhook.service" "$WEBHOOK_SERVICE_DEST/webhook.service"
  systemctl enable webhook.service
  systemctl start webhook.service
else
  echo "webhook.service symlink already exists.  Skipping symlink creation."
  systemctl daemon-reload
  if [ "$(systemctl is-active --quiet webhook.service)" ]; then
    echo "Restarting webhook.service..."
    systemctl restart webhook.service
  else 
    echo "Starting webhook.service..."
    systemctl start webhook.service
  fi
fi

