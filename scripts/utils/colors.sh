#!/bin/sh
black="\e[30m"
red="\e[31m"
green="\e[32m"
brown="\e[33m"
blue="\e[34m"
purple="\e[35m"
cyan="\e[36m"
gray="\e[37m"
end="\e[0m"

bold="\e[1m"
underline="\e[3m"
