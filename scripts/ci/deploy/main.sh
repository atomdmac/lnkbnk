#!/bin/sh
 
# Declare our intentions.
echo "Deploying!"

# Source production environment secrets
. ./.env

# Stop currently running services.
docker-compose down && \
  # Pull the latest container images and spin them up.
  docker-compose pull && \
  docker-compose up -d

# Report our glorious victory.
echo "Deploy success!"
