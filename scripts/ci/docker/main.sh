#!/bin/bash

# Build and push docker images for images that support CI.
build_and_push () {
  NAME=$1
  COMMIT=$(git log -n 1 --pretty=format:"%h")
  TAG=${2:-$COMMIT}
  QUALIFIED_NAME="lnkbnk_$NAME"
  FULL_NAME="adammacumber/$QUALIFIED_NAME:$TAG"
  DOCKERFILE_PATH="./docker/ci/$NAME/"
  docker buildx build "$DOCKERFILE_PATH" \
    -t "$FULL_NAME" \
    --platform linux/amd64
  # TODO: Figure out how to do multi-arch image builds...
 # docker buildx build "$DOCKERFILE_PATH" \
  #   -t "$FULL_NAME" \
  #   --platform linux/aarch64
  docker push "$FULL_NAME"
}

if [ "$1" = "all" ]; then
  build_and_push notify "$@"
  build_and_push e2e "$@"
else
  build_and_push "$@"
fi

