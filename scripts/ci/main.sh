#!/bin/sh

# Relative to project root
. ./scripts/utils/colors.sh

MOD_DIR=$(dirname "$0")

COMMAND=$1
if [ $# -gt 0 ]; then
  shift
fi

print_help () {
  echo "Usage: asdf ci [command]"
  echo ""
  # List commands
  printf "${bold}${cyan}COMMANDS:${end}\n"
  printf "  ${bold}deploy${end}\t Deploy containers on production\n"
  printf "  ${bold}docker${end}\t Build & push suporting Docker images\n"
  printf "  ${bold}woodpecker${end}\t Interact with the build pipeline using woodpecker-cli\n"
  echo ""
}

case "$COMMAND" in
  deploy | dep) "$MOD_DIR/deploy/main.sh" "$@";;
  docker | doc) "$MOD_DIR/docker/main.sh" "$@";;
  woodpecker | wp) woodpecker-cli "$@";;
  *) print_help;;
esac
