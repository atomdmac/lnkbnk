import { Link } from "@remix-run/react/components";

export default function Breadcrumbs({ pathStr }: { pathStr: string }) {
  const separator = <li>/</li>;
  const segments = pathStr
    .split("/")
    .filter((s) => !!s)
    .reduce(
      (
        output: JSX.Element[],
        current: string,
        index: number,
        originalPathStr: string[]
      ) => {
        const currentPath = originalPathStr.slice(0, index + 1).join("/");
        output.push(
          separator,
          <li key={index}>
            <Link to={currentPath}>{current}</Link>
          </li>
        );
        return output;
      },
      []
    );
  segments.unshift(
    <li key={"root"}>
      <Link to={"."}>/root</Link>
    </li>
  );
  return <nav className="flex list-none">{segments}</nav>;
}
