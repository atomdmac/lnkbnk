import { useField } from "remix-validated-form";

export default function FormError({ name }: { name: string }) {
  const { error } = useField(name);
  if (!error) return null;
  return <div className="mt-2 text-red-500">{error}</div>;
}
