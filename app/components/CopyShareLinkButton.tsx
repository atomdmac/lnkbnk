import type { Share } from "@prisma/client";
import { copyDownloadLink } from "~/utils";
import ContextMenuButton from "./ContextMenuButton";

export const CopyShareLinkButton = ({
  share,
  children,
  className,
}: {
  share: Share;
  children: React.ReactChild;
  className?: string;
}) => {
  return (
    <ContextMenuButton
      className={className}
      onClick={() => copyDownloadLink(share.id)}
    >
      {children}
    </ContextMenuButton>
  );
};
