export default function Label(
  props: React.DetailedHTMLProps<
    React.LabelHTMLAttributes<HTMLLabelElement>,
    HTMLLabelElement
  >
) {
  return (
    <label {...props} className={`${props.className} mb-1 block`}>
      {props.children}
    </label>
  );
}
