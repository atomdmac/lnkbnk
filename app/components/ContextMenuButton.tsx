import type { ButtonHTMLAttributes } from "react";

export const ContextMenuButton = (props: ButtonHTMLAttributes<{}>) => {
  return (
    <button {...props} className={`w-full text-left ${props.className}`}>
      {props.children}
    </button>
  );
};

export default ContextMenuButton;
