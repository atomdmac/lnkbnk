type DateDisplayProps = {
  date: Date;
} & React.HTMLAttributes<HTMLDivElement>;

const DateDisplay = (props: DateDisplayProps) => {
  const { date } = props;
  const dateObj = new Date(date);
  const display = `${dateObj.getDate()}/${dateObj.getMonth()}/${dateObj.getFullYear()}`;
  return <span {...props}>{display}</span>;
};

export default DateDisplay;
