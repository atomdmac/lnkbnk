import { Link } from "@remix-run/react";
import type { ShareableDirectoryEntry } from "~/lib/shares";

export const style = {
  table: {
    width: "100%",
  },
  td: {
    borderRight: "1px solid black",
  },
};

export const Row = ({ dirEntry }: { dirEntry: ShareableDirectoryEntry }) => {
  return (
    <tr className="border-t border-slate-900">
      <td className="py-5 ">
        {dirEntry.isDirectory ? (
          <Link to={`${dirEntry.pathToBrowse}`}>{dirEntry.baseName}</Link>
        ) : (
          dirEntry.baseName
        )}
      </td>
      <td className="text-right">
        {!dirEntry.isDirectory && (
          <Link to={dirEntry.pathToDownload} reloadDocument>
            Download
          </Link>
        )}
      </td>
    </tr>
  );
};

export default function Index({
  dirEntry,
}: {
  dirEntry: ShareableDirectoryEntry;
}) {
  let shareEls = dirEntry.children
    ? dirEntry.children.map((share: ShareableDirectoryEntry, index: number) => (
        <Row key={index} dirEntry={share} />
      ))
    : [];

  return (
    <table style={{ width: "100%" }}>
      <thead>
        <tr>
          <th className="pb-3 text-left">Path</th>
          <th className="pb-3 text-right">Actions</th>
        </tr>
      </thead>
      <tbody>{shareEls}</tbody>
    </table>
  );
}
