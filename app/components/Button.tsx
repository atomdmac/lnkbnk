export default function Button(
  props: React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  >
) {
  return (
    <button
      className={`${
        props.className || ""
      } mt-5 rounded-md border-2 border-green-500 bg-green-600 py-1 px-4 text-white`}
    >
      {props.children}
    </button>
  );
}
