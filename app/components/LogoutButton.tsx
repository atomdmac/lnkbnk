const LogoutButton = ({ className }: { className?: string }) => {
  return (
    <form className="block" method="post" action="/logout">
      <button
        className={`w-full text-left text-slate-500 ${className}`}
        type="submit"
      >
        Logout
      </button>
    </form>
  );
};

export default LogoutButton;
