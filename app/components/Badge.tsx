export type BadgeProps = {
  children: JSX.Element | JSX.Element[] | number | string | (string | number)[];
  className?: string;
};

export default function Badge({ children, className = "" }: BadgeProps) {
  return (
    <div
      className={`${className} rounded-sm border-2 border-slate-300 px-2 py-1 text-sm`}
    >
      {children}
    </div>
  );
}
