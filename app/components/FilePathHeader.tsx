import H1 from "./H1";

export default function FilePathHeader({
  children,
}: {
  children: JSX.Element;
}) {
  return <H1 className="mb-5 text-xl">{children}</H1>;
}
