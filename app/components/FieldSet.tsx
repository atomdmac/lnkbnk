export default function FieldSet(
  props: React.DetailedHTMLProps<
    React.FieldsetHTMLAttributes<HTMLFieldSetElement>,
    HTMLFieldSetElement
  >
) {
  return (
    <fieldset className={`${props.className || ""} mb-4 flex-row`}>
      {props.children}
    </fieldset>
  );
}
