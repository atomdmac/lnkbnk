import type { User } from ".prisma/client";
import { useActionData } from "@remix-run/react";
import { useEffect } from "react";
import { useFormContext } from "remix-validated-form";
import Button from "./Button";
import FieldSet from "./FieldSet";
import FormError from "./FormError";
import Input from "./Input";
import Label from "./Label";

export default function NewUserForm() {
  const actionData = useActionData<User>();
  const form = useFormContext();
  useEffect(() => {
    if (actionData && form.hasBeenSubmitted && !form.isSubmitting) {
      form.reset();
      alert("User created!");
    }
  }, [actionData, form]);
  return (
    <>
      <FieldSet>
        <Label htmlFor="email">Email:</Label>
        <Input type="email" name="email" defaultValue="" />
        <FormError name="email" />
      </FieldSet>
      <FieldSet>
        <Label htmlFor="password">New Password:</Label>
        <Input type="password" name="password" defaultValue="" />
        <FormError name="password" />
      </FieldSet>
      <FieldSet>
        <Label htmlFor="passwordConfirm">Confirm Password:</Label>
        <Input type="password" name="passwordConfirm" defaultValue="" />
        <FormError name="passwordConfirm" />
      </FieldSet>
      <FieldSet>
        <Label htmlFor="syncthingDeviceId">Syncthing Device ID</Label>
        <Input type="text" name="syncthingDeviceId" defaultValue="" />
        <FormError name="syncthingDeviceId" />
      </FieldSet>
      <Button type="submit" disabled={form.isSubmitting}>
        Create User
      </Button>
    </>
  );
}
