export default function H1({
  children,
  className = "",
}: {
  children: React.ReactNode;
  className?: string;
}) {
  return <h1 className={`${className} text-3xl leading-loose`}>{children}</h1>;
}
