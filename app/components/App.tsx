import React from "react";
import LNKBNKLogo from "./LNKBNKLogo";
import DotMenu from "./DotMenu";

type AppProps = React.PropsWithChildren<{
  leftMenu: React.ReactNode;
  rightMenu: React.ReactNode;
  mobileMenu: React.ReactNode;
}>;

export default function App({
  children,
  leftMenu,
  rightMenu,
  mobileMenu,
}: AppProps) {
  return (
    <div className="container px-5">
      <header className="container my-5 flex items-center justify-between border-b-2 border-slate-200 pb-3">
        <span className="flex items-center">
          <span className="mr-5">
            <LNKBNKLogo className="text-2xl" />
          </span>
          <nav className="hidden md:inline">{leftMenu}</nav>
        </span>
        <div className="hidden flex-row md:flex">{rightMenu}</div>
        <DotMenu className="block md:hidden">{mobileMenu}</DotMenu>
      </header>
      {children}
    </div>
  );
}
