import { NavLink } from "@remix-run/react";
import type { RemixNavLinkProps } from "@remix-run/react/components";

export default function NavItem({ children, ...rest }: RemixNavLinkProps) {
  const className = ({ isActive }: { isActive: boolean }) =>
    `${
      isActive
        ? "cursor-default before:content-['$_'] before:font-bold before:text-slate-500"
        : "text-slate-500"
    } mr-5`;
  return (
    <NavLink {...rest} className={className}>
      {children}
    </NavLink>
  );
}
