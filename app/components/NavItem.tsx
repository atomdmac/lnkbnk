import { NavLink } from "@remix-run/react";
import type { RemixNavLinkProps } from "@remix-run/react/components";

export default function NavItem({ children, ...rest }: RemixNavLinkProps) {
  const className = ({ isActive }: { isActive: boolean }) =>
    `${isActive ? "cursor-default" : "text-slate-500"} mr-5`;
  return (
    <NavLink {...rest} className={className}>
      {children}
    </NavLink>
  );
}
