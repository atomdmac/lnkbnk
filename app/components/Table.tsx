import React from "react";

export const Table: React.FC = (props) => {
  return (
    <table className="w-full" {...props}>
      {props.children}
    </table>
  );
};

export const TH: React.FC<
  React.DetailedHTMLProps<
    React.TdHTMLAttributes<HTMLTableHeaderCellElement>,
    HTMLTableHeaderCellElement
  >
> = (props) => {
  return (
    <th
      {...props}
      className={`pb-3 text-left last:text-right ${props.className}`}
    >
      {props.children}
    </th>
  );
};

export const TR: React.FC = (props) => {
  return <tr {...props}>{props.children}</tr>;
};

export const TD: React.FC<
  React.DetailedHTMLProps<
    React.TdHTMLAttributes<HTMLTableDataCellElement>,
    HTMLTableDataCellElement
  >
> = (props) => {
  return (
    <td
      {...props}
      className={`border-t-2 border-slate-700 py-4 last:text-right ${props.className}`}
    >
      {props.children}
    </td>
  );
};
