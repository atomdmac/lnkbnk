export default function Input(
  props: React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  >
) {
  return (
    <input
      {...props}
      className={`${props.className} inline-block w-full rounded-sm border-2 py-1 px-2`}
    />
  );
}
