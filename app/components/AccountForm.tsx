import { ValidatedForm } from "remix-validated-form";
import Button from "./Button";
import FieldSet from "./FieldSet";
import FormError from "./FormError";
import Label from "./Label";
import Input from "./Input";
import type { Validator } from "remix-validated-form";
import type { User } from "@prisma/client";

export default function AccountForm({
  validator,
  user,
}: {
  validator: Validator<unknown>;
  user?: User;
}) {
  const userId = user && user.id;
  const userEmail = user && user.email;
  return (
    <ValidatedForm
      method="post"
      className="container mx-auto"
      validator={validator}
    >
      <input type="text" name="userId" hidden defaultValue={userId} />
      <FieldSet>
        <Label htmlFor="email">Email:</Label>
        <Input type="email" name="email" defaultValue={userEmail} />
        <FormError name="email" />
      </FieldSet>
      <FieldSet>
        <Label htmlFor="password">New Password:</Label>
        <Input type="password" name="password" defaultValue="" />
        <FormError name="password" />
      </FieldSet>
      <FieldSet>
        <Label htmlFor="passwordConfirm">Confirm Password:</Label>
        <Input type="password" name="passwordConfirm" defaultValue="" />
        <FormError name="passwordConfirm" />
      </FieldSet>
      <Button type="submit">Save Changes</Button>
    </ValidatedForm>
  );
}
