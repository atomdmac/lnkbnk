import React from "react";

type DotMenuProps = React.PropsWithChildren<{
  className?: string;
}>;

export function DotIcon() {
  const dots = Array(3)
    .fill(undefined)
    .map((_, index) => (
      <div
        key={index}
        style={{ width: ".7em", height: ".1em" }}
        className="self-center rounded-sm bg-white"
      ></div>
    ));
  return (
    <div
      className="peer m-0 h-10 w-10 rounded-3xl border-white bg-zinc-500 p-2"
      tabIndex={1}
    >
      <div className="flex h-full flex-col justify-around">{dots}</div>
    </div>
  );
}

export default function DotMenu({ children, className = "" }: DotMenuProps) {
  // Unwrap the children from the slot.
  const nestedChildren = children as React.ReactElement;
  return (
    <div className={`${className} relative inline-block cursor-pointer`}>
      <DotIcon />
      <div
        tabIndex={1}
        className="invisible absolute right-0 top-0 z-50 w-max select-none rounded-sm bg-white p-1 text-slate-800 active:visible peer-focus:visible"
      >
        {React.Children.map(nestedChildren, (Child) =>
          React.cloneElement(Child, {
            ...Child.props,
            className: "p-2 block hover:bg-slate-800 hover:text-white",
          })
        )}
      </div>
    </div>
  );
}
