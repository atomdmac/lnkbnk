const LNKBNKLogo = (props: React.HTMLAttributes<HTMLDivElement>) => {
  const combinedClasses = ` tracking-widest font-black ${props.className}`;
  return (
    <h1 {...props} className={combinedClasses}>
      <span className="text-slate-50">LNK</span>
      <span className="text-slate-500">BNK</span>
    </h1>
  );
};

export default LNKBNKLogo;
