import { Link } from "@remix-run/react";
import H1 from "./H1";

export default function ErrorBoundary({ error }: { error: Error }) {
  return (
    <div className="flex flex-col items-center">
      <H1>Oof</H1>
      <p className="justify-center">An error occurred: "{error.message}"</p>
      <Link to="/">Back to Home</Link>
    </div>
  );
}
