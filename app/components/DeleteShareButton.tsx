import type { Share } from ".prisma/client";
import { useFetcher } from "@remix-run/react";
import ContextMenuButton from "~/components/ContextMenuButton";

export const DeleteShareButton = ({
  share,
  className,
}: {
  share: Share;
  className?: string;
}) => {
  const fetcher = useFetcher();
  return (
    <fetcher.Form method="post" action="/admin/shares/delete">
      <input hidden type="text" name="shareId" defaultValue={share.id} />
      <ContextMenuButton className={`${className}`} type="submit">
        Delete
      </ContextMenuButton>
    </fetcher.Form>
  );
};
