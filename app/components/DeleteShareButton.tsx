import type { Share } from ".prisma/client";
import { useFetcher } from "@remix-run/react";
import type { FormEvent } from "react";
import ContextMenuButton from "~/components/ContextMenuButton";

export const DeleteShareButton = ({
  share,
  className,
}: {
  share: Share;
  className?: string;
}) => {
  const fetcher = useFetcher();
  const confirmAction = (event: FormEvent) => {
    const confirmed = confirm("Press 'OK' to Confirm Deletion");
    if (confirmed) {
      return event;
    } else {
      event.preventDefault();
    }
  };
  return (
    <fetcher.Form
      method="post"
      action="/admin/shares/delete"
      onSubmit={confirmAction}
    >
      <input hidden type="text" name="shareId" defaultValue={share.id} />
      <ContextMenuButton className={`${className}`} type="submit">
        Delete
      </ContextMenuButton>
    </fetcher.Form>
  );
};
