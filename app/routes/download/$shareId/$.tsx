import type { LoaderFunction } from "@remix-run/server-runtime";
import { json } from "@remix-run/server-runtime";
import invariant from "tiny-invariant";
import fs from "node:fs/promises";
import { getShareById } from "~/models/share.server";
import path from "node:path";
import { getAbsolutePath } from "~/lib/file-io";

export const loader: LoaderFunction = async ({ params }) => {
  const { "*": shareFilePath = "", shareId } = params;

  invariant(shareId, "shareId is required");

  const share = await getShareById(shareId);

  if (!share) {
    return json({}, 404);
  }

  const target = path.join(getAbsolutePath(), share.path, shareFilePath);

  const file = await fs.readFile(target);
  const fileName = path.basename(target);

  return new Response(file, {
    status: 200,
    headers: {
      "Content-Type": "application/blob",
      "Content-Disposition": `attachment; filename="${fileName}"`,
    },
  });
};
