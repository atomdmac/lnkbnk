import type { LoaderFunction } from "@remix-run/node";
import { Link, Outlet, useLoaderData } from "@remix-run/react";
import App from "~/components/App";
import LogoutButton from "~/components/LogoutButton";
import NavItem from "~/components/NavItem";
import { requireUserId } from "~/session.server";

export const loader: LoaderFunction = async ({ request }) => {
  return await requireUserId(request);
};

export const LeftMenu = () => {
  return (
    <>
      <NavItem key="browse" to="files">
        Files
      </NavItem>
      <NavItem key="manage-shares" to="shares">
        Shares
      </NavItem>
      <NavItem key="manage-users" to="users">
        Users
      </NavItem>
    </>
  );
};

export const RightMenu = () => {
  return (
    <>
      <NavItem to="account">Account</NavItem>
      <LogoutButton />
    </>
  );
};

export const MobileMenu = () => [
  <Link key="browse" to="files">
    Files
  </Link>,
  <Link key="manage-shares" to="shares">
    Shares
  </Link>,
  <Link key="manage-users" to="users">
    Users
  </Link>,
  <Link key="account" to="account">
    Account
  </Link>,
  <LogoutButton key="logout" />,
];

export default function Index() {
  useLoaderData();
  return (
    <App
      leftMenu={<LeftMenu />}
      rightMenu={<RightMenu />}
      mobileMenu={MobileMenu()}
    >
      <Outlet />
    </App>
  );
}
