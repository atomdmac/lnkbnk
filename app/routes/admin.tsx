import type { LoaderFunction } from "@remix-run/node";
import { Link, Outlet, useLoaderData } from "@remix-run/react";
import App from "~/components/App";
import LogoutButton from "~/components/LogoutButton";
import NavItem from "~/components/NavItem";
import { requireUserId } from "~/session.server";

export const loader: LoaderFunction = async ({ request }) => {
  return await requireUserId(request);
};

export const LeftMenu = () => {
  return (
    <>
      <NavItem key="browse" to="files">
        Browse Files
      </NavItem>
      <NavItem key="manage-shares" to="shares">
        Manage Shares
      </NavItem>
    </>
  );
};

export const RightMenu = () => {
  return (
    <>
      <NavItem to="account">Account</NavItem>
      <LogoutButton />
    </>
  );
};

export const MobileMenu = () => [
  <Link key="browse" to="files">
    Browse Files
  </Link>,
  <Link key="manage-shares" to="shares">
    Manage Shares
  </Link>,
  <Link key="account" to="account">
    Account
  </Link>,
  <LogoutButton key="logout" />,
];

export default function Index() {
  useLoaderData();
  return (
    <App
      leftMenu={<LeftMenu />}
      rightMenu={<RightMenu />}
      mobileMenu={MobileMenu()}
    >
      <Outlet />
    </App>
  );
}
