import type { LoaderFunction } from "@remix-run/node";
import { json } from "@remix-run/node";
import { Link, useLoaderData } from "@remix-run/react";
import FilePathHeader from "~/components/FilePathHeader";
import { Table, TD, TH, TR } from "~/components/Table";
import type { ShareableDirectoryEntrySummary } from "~/lib/shares";
import { createShareableDirectoryEntrySummary } from "~/lib/shares";
import { getAdminShares } from "~/models/share.server";

export const loader: LoaderFunction = async (): Promise<Response> => {
  const adminShares = await getAdminShares();
  const data = adminShares.map((a) => createShareableDirectoryEntrySummary(a));
  return json(data);
};

export default function Index() {
  const shares = useLoaderData<ShareableDirectoryEntrySummary[]>();
  const list = shares.map((s) => (
    <TR key={s.pathRelativeToRoot}>
      <TD className="py-0">
        <Link className="block py-2" to={s.pathToBrowse}>
          {s.pathRelativeToRoot}
        </Link>
      </TD>
      <TD>{s.count}</TD>
    </TR>
  ));

  return (
    <>
      <FilePathHeader>
        <div>All Shares</div>
      </FilePathHeader>
      <Table>
        <thead>
          <TH>Path</TH>
          <TH># of Shares</TH>
        </thead>
        <tbody>{list}</tbody>
      </Table>
    </>
  );
}
