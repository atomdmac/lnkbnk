import type { ActionFunction } from "@remix-run/server-runtime";
import { json } from "@remix-run/server-runtime";
import invariant from "tiny-invariant";
import logging from "~/lib/logging";
import { deleteShare } from "~/models/share.server";

export const action: ActionFunction = async ({
  request,
}): Promise<Response> => {
  const data = await request.formData();
  const shareId = data.get("shareId")?.toString();
  invariant(shareId, "shareId is required");

  try {
    logging.debug(`Deleting share with ID ${shareId}`);
    await deleteShare(shareId);
  } catch (error) {
    logging.error(error);
    return json({ message: "Error deleting share" }, 500);
  }

  return new Response(null, {
    status: 200,
  });
};
