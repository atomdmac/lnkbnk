import type { LoaderFunction } from "@remix-run/node";
import { json, redirect } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import { CopyShareLinkButton } from "~/components/CopyShareLinkButton";
import DateDisplay from "~/components/DateDisplay";
import { DeleteShareButton } from "~/components/DeleteShareButton";
import DotMenu from "~/components/DotMenu";
import FilePathHeader from "~/components/FilePathHeader";
import { Table, TD, TR } from "~/components/Table";
import type { Share } from "~/models/share.server";
import { getShares } from "~/models/share.server";

export const loader: LoaderFunction = async ({ params }): Promise<Response> => {
  const path = params["*"];
  if (!path) {
    return redirect("/admin/shares/all");
  }
  const data = await getShares(path);
  if (!data.length) {
    return redirect("/admin/shares/all");
  }
  return json(data);
};

export default function Index() {
  const shares = useLoaderData<Share[]>();
  let rows = shares.map((share: Share) => (
    <TR key={share.id}>
      <TD className="hidden md:block">{share.id}</TD>
      <TD>
        <DateDisplay date={share.createdAt}></DateDisplay>
      </TD>
      <TD>
        <DateDisplay date={share.expiresAt}></DateDisplay>
      </TD>
      <TD align="right">
        <DotMenu className="align-middle">
          <CopyShareLinkButton share={share}>Copy Link</CopyShareLinkButton>
          <DeleteShareButton share={share} />
        </DotMenu>
      </TD>
    </TR>
  ));

  return (
    <div>
      <FilePathHeader>
        <div>All Shares for {shares[0].path}</div>
      </FilePathHeader>
      <Table>
        <thead>
          <tr>
            <th className="hidden pb-3 text-left md:block">Token</th>
            <th className="pb-3 text-left">Created At</th>
            <th className="pb-3 text-left">Expires At</th>
            <th className="pb-3 text-right">Actions</th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </Table>
    </div>
  );
}
