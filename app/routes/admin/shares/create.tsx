import { json } from "@remix-run/node";
import type { ActionFunction } from "@remix-run/server-runtime";
import invariant from "tiny-invariant";
import logger from "~/lib/logging";
import { createShare } from "~/models/share.server";

export const action: ActionFunction = async ({
  request,
}): Promise<Response> => {
  const data = await request.formData();
  const pathParam = data.get("path")?.toString();

  invariant(pathParam, "Path is required");

  try {
    logger.debug(`Creating new share with path "${pathParam}"`);
    const share = await createShare({ path: pathParam });
    return json(share);
  } catch (error) {
    logger.error(error);
    return json({ message: "Error creating share" }, 404);
  }
};
