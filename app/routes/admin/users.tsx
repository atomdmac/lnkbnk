import { Outlet } from "@remix-run/react";
import NavItem from "~/components/NavItem";
export { default as ErrorBoundary } from "~/components/ErrorBoundary";

export default function ManageUsers() {
  return (
    <>
      <nav className="mb-7 border-b-2 border-b-white pb-4">
        <NavItem to="list">List</NavItem>
        <NavItem to="create">Create</NavItem>
      </nav>
      <Outlet />
    </>
  );
}
