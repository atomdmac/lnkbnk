import type { ActionFunction } from "@remix-run/server-runtime";
import { redirect } from "@remix-run/server-runtime";
import { withZod } from "@remix-validated-form/with-zod";
import invariant from "tiny-invariant";
import { z } from "zod";
import { zfd } from "zod-form-data";
import AccountForm from "~/components/AccountForm";
import H1 from "~/components/H1";
import logger from "~/lib/logging";
import { updateCredentials } from "~/models/user.server";
import { useUser } from "~/utils";

export const action: ActionFunction = async ({ request }) => {
  const formData = await request.formData();
  const id = formData.get("userId")?.toString();
  const email = formData.get("email")?.toString();
  const password = formData.get("password")?.toString();

  invariant(id, "User ID is required");
  invariant(email, "Email is required");
  invariant(password, "Password is required");

  try {
    await updateCredentials({
      id,
      email,
      password,
    });
  } catch (error) {
    logger.error(error);
    return new Response(null, { status: 500 });
  }

  return redirect("/admin");
};

export const schema = z
  .object({
    email: z.string().email(),
    password: z.string().min(12),
    passwordConfirm: z.string().min(12),
  })
  .refine((data) => data.password === data.passwordConfirm, {
    message: "Passwords gotta match, dude.",
    path: ["passwordConfirm"],
  });

export const clientValidator = withZod(schema);
export const serverValidator = zfd.formData(schema);

export default function Account() {
  const user = useUser();

  return (
    <>
      <H1>Account Settings</H1>
      <AccountForm validator={clientValidator} user={user} />
    </>
  );
}
