import type { Share } from "@prisma/client";
import { json } from "@remix-run/node";
import { Link, useFetcher, useLoaderData } from "@remix-run/react";
import type { LoaderFunction } from "@remix-run/server-runtime";
import { useEffect } from "react";
import Badge from "~/components/Badge";
import Breadcrumbs from "~/components/Breadcrumbs";
import DotMenu from "~/components/DotMenu";
import FilePathHeader from "~/components/FilePathHeader";
import { Table, TD, TH, TR } from "~/components/Table";
import type { FullDirectoryEntry } from "~/lib/file-io";
import { readDirectoryEntry } from "~/lib/file-io";
import type { ShareableDirectoryEntrySummary } from "~/lib/shares";
import { createShareableDirectoryEntrySummary } from "~/lib/shares";
import { getAdminShares } from "~/models/share.server";
import { copyDownloadLink, Sorters } from "~/utils";

export type FullDirectoryEntryWithShareSummary = {
  directoryEntry: FullDirectoryEntry;
  summary: ShareableDirectoryEntrySummary;
  children: FullDirectoryEntryWithShareSummary[];
};

/**
 * Given a path relative to the root share directory, return a
 * FullDirectoryEntryWithShareSummary.  If there are no shares associated with
 */
export const getShareSummary = async (pathRelativeToRoot: string) =>
  getAdminShares(pathRelativeToRoot).then((shares) =>
    shares.length
      ? createShareableDirectoryEntrySummary(shares[0])
      : {
          pathRelativeToRoot: pathRelativeToRoot,
          pathToBrowse: pathRelativeToRoot,
          count: 0,
        }
  );

export const loader: LoaderFunction = async ({ params }): Promise<Response> => {
  const relativePathToOpen = params["*"] || "";

  // Get all items in the current directory
  const topDirEntry = await readDirectoryEntry({ relativePathToOpen });
  topDirEntry.children?.sort(Sorters.byDirectory);

  // Associate summaries with directory entries (ex. share count, etc.)
  const withSummaries: FullDirectoryEntryWithShareSummary[] = await Promise.all(
    (topDirEntry.children || []).map(async (d) => ({
      directoryEntry: d,
      summary: await getShareSummary(d.pathRelativeToRoot),
      children: [],
    }))
  );

  // Associate summary with top-level/current directory entry
  const currentDirectoryEntry: FullDirectoryEntryWithShareSummary = {
    directoryEntry: topDirEntry,
    summary: await getShareSummary(topDirEntry.pathRelativeToRoot),
    children: withSummaries,
  };

  return json(currentDirectoryEntry);
};

const Row = ({
  directoryEntryWithSummary,
}: {
  directoryEntryWithSummary: FullDirectoryEntryWithShareSummary;
}) => {
  const {
    directoryEntry: { baseName, isDirectory, pathRelativeToRoot },
    summary: { count: shareCount },
  } = directoryEntryWithSummary;
  const fetcher = useFetcher<Share>();

  // Copy the download link once it has been created.
  useEffect(() => {
    if (fetcher.data) {
      copyDownloadLink(fetcher.data.id);
    }
  }, [fetcher.data]);

  return (
    <TR>
      <TD>
        {isDirectory ? (
          <Link to={pathRelativeToRoot}>{baseName}</Link>
        ) : (
          <span>{baseName}</span>
        )}
      </TD>
      <TD>
        <div className="flex items-center justify-end">
          {shareCount ? (
            <Badge className="mr-2">Shares: {shareCount}</Badge>
          ) : (
            ""
          )}
          <DotMenu>
            <Link to={`/admin/shares/${pathRelativeToRoot}`}>
              Manage Shares
            </Link>
            <fetcher.Form method="post" action="/admin/shares/create">
              <input
                hidden
                type="text"
                name="path"
                defaultValue={pathRelativeToRoot}
              />
              <button type="submit">Share ({shareCount})</button>
            </fetcher.Form>
          </DotMenu>
        </div>
      </TD>
    </TR>
  );
};

export default function Index() {
  const currentDirectoryEntry =
    useLoaderData<FullDirectoryEntryWithShareSummary>();

  const rows = (currentDirectoryEntry.children || []).map(
    (dirEntry: FullDirectoryEntryWithShareSummary) => {
      return (
        <Row
          key={`${dirEntry.directoryEntry.dirNameRelativeToRoot}/${dirEntry.directoryEntry.baseName}`}
          directoryEntryWithSummary={dirEntry}
        />
      );
    }
  );

  return (
    <>
      <FilePathHeader>
        <Breadcrumbs
          pathStr={currentDirectoryEntry.directoryEntry.dirNameRelativeToRoot}
        />
      </FilePathHeader>
      <Table>
        <thead>
          <TH>Filename</TH>
          <TH>Actions</TH>
        </thead>
        <tbody>{rows}</tbody>
      </Table>
    </>
  );
}
