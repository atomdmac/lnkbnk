import type { DirectoryEntry } from "~/file-io";

export type BrowsableItem = {
  browsePath: string;
  downloadPath: string;
};

export type BrowsableDirectoryEntry = BrowsableItem & DirectoryEntry;
