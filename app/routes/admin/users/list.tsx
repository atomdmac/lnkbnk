import { useFetcher, useLoaderData } from "@remix-run/react";
import type { LoaderFunction } from "@remix-run/server-runtime";
import type { FormEvent } from "react";
import { ContextMenuButton } from "~/components/ContextMenuButton";
import { Table, TD, TH, TR } from "~/components/Table";
import type { User } from "~/models/user.server";
import { getAllUsers } from "~/models/user.server";
export { default as ErrorBoundary } from "~/components/ErrorBoundary";

export const loader: LoaderFunction = async () => {
  return await getAllUsers();
};

export const DeleteUserButton = ({
  user,
  className,
}: {
  user: User;
  className?: string;
}) => {
  const fetcher = useFetcher();
  const confirmAction = (event: FormEvent) => {
    const confirmed = confirm("Press 'OK' to Confirm Deletion");
    if (confirmed) {
      return event;
    } else {
      event.preventDefault();
    }
  };
  return (
    <fetcher.Form
      method="post"
      action="/admin/users/delete"
      onSubmit={confirmAction}
    >
      <input hidden type="text" name="email" defaultValue={user.email} />
      <ContextMenuButton className={`${className}`} type="submit">
        Delete
      </ContextMenuButton>
    </fetcher.Form>
  );
};

export default function ListUsers() {
  const users = useLoaderData<User[]>();
  const list = users.map((user) => (
    <TR key={user.email}>
      <TD className="py-2">{user.email}</TD>
      <TD>
        <DeleteUserButton
          className="text-right"
          user={{
            ...user,
            createdAt: new Date(user.createdAt),
            updatedAt: new Date(user.updatedAt),
          }}
        />
      </TD>
    </TR>
  ));

  return (
    <div className="overflow-x-auto">
      <Table>
        <thead>
          <TH>User</TH>
          <TH>Actions</TH>
        </thead>
        <tbody>{list}</tbody>
      </Table>
    </div>
  );
}
