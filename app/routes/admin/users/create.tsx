import type { ActionFunction } from "@remix-run/server-runtime";
import { json } from "@remix-run/server-runtime";
import { withZod } from "@remix-validated-form/with-zod";
import { ValidatedForm } from "remix-validated-form";
import { z } from "zod";
import { zfd } from "zod-form-data";
import H1 from "~/components/H1";
import NewUserForm from "~/components/NewUserForm";
import { createSyncthingDeviceId } from "~/models/syncthing-device-id.server";
import { createUser } from "~/models/user.server";
export { default as ErrorBoundary } from "~/components/ErrorBoundary";

export const schema = z
  .object({
    email: z.string().email(),
    password: z.string().min(12),
    passwordConfirm: z.string().min(12),
    syncthingDeviceId: z.string(),
  })
  .refine((data) => data.password === data.passwordConfirm, {
    message: "Passwords gotta match, dude.",
    path: ["passwordConfirm"],
  });

export const clientValidator = withZod(schema);
export const serverValidator = zfd.formData(schema);

export const action: ActionFunction = async ({ request }) => {
  const formData = await request.formData();
  const { email, password, syncthingDeviceId } =
    serverValidator.parse(formData);

  const user = await createUser(email, password);
  await createSyncthingDeviceId(user.id, syncthingDeviceId);
  return json(user, { status: 201, statusText: "ok" });
};

export default function NewUser() {
  return (
    <>
      <H1>Create User</H1>

      <ValidatedForm
        method="post"
        className="container mx-auto"
        validator={clientValidator}
      >
        <NewUserForm />
      </ValidatedForm>
    </>
  );
}
