import type { ActionFunction } from "@remix-run/server-runtime";
import { json } from "@remix-run/node";
import invariant from "tiny-invariant";
import logging from "~/lib/logging";
import { deleteUserByEmail } from "~/models/user.server";

export const action: ActionFunction = async ({
  request,
}): Promise<Response> => {
  const data = await request.formData();
  const email = data.get("email")?.toString();
  invariant(email, "email is required");

  try {
    logging.debug(`Deleting user with email ${email}`);
    await deleteUserByEmail(email);
  } catch (error) {
    logging.error(error);
    return json({ message: "Error deleting email" }, 500);
  }

  return new Response(null, {
    status: 200,
  });
};
