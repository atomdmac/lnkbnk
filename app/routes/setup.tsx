import type { ActionFunction, LoaderFunction } from "@remix-run/server-runtime";
import { redirect } from "@remix-run/server-runtime";
import { withZod } from "@remix-validated-form/with-zod";
import { z } from "zod";
import { zfd } from "zod-form-data";
import AccountForm from "~/components/AccountForm";
import { createUser, getUserCount } from "~/models/user.server";
import { createUserSession } from "~/session.server";

export const schema = z
  .object({
    email: z.string().email(),
    password: z.string().min(12),
    passwordConfirm: z.string().min(12),
  })
  .refine((data) => data.password === data.passwordConfirm, {
    message: "Passwords gotta match, dude.",
    path: ["passwordConfirm"],
  });

export const clientValidator = withZod(schema);
export const serverValidator = zfd.formData(schema);

export const loader: LoaderFunction = async () => {
  const userCount = await getUserCount();
  if (userCount > 0) {
    return redirect("/");
  }
  return new Response();
};

export const action: ActionFunction = async ({ request }) => {
  const data = serverValidator.parse(await request.formData());
  const user = await createUser(data.email, data.password);
  return createUserSession({
    request,
    userId: user.id,
    remember: false,
    redirectTo: `/admin`,
  });
};

export default function Setup() {
  return <AccountForm validator={clientValidator} />;
}
