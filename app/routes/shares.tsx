import { Outlet } from "@remix-run/react";
import App from "~/components/App";

export default function Shares() {
  return (
    <App showLogout={false}>
      <Outlet />
    </App>
  );
}
