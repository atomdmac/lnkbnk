import { useLoaderData } from "@remix-run/react";
import { redirect } from "@remix-run/server-runtime";
import type { LoaderFunction } from "@remix-run/server-runtime/routeModules";
import H1 from "~/components/H1";
import { getUserId } from "~/session.server";

export const loader: LoaderFunction = async ({ request }) => {
  const userId = await getUserId(request);
  // If the user is logged in, forward them to their admin dashboard.
  if (userId) return redirect("/admin");
  return new Response();
};

export default function Index() {
  useLoaderData();
  return (
    <main className="relative min-h-screen flex-col bg-white sm:flex sm:items-center sm:justify-center">
      <H1>Are you lost?</H1>
      <p>Move along.</p>
    </main>
  );
}
