import type { LoaderFunction } from "@remix-run/node";
import { json, redirect } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import path from "path";
import Breadcrumbs from "~/components/Breadcrumbs";
import FilePathHeader from "~/components/FilePathHeader";
import SharesTable from "~/components/SharesTable";
import { readDirectoryEntry } from "~/lib/file-io";
import type { ShareableDirectoryEntry } from "~/lib/shares";
import { createShareableDirectoryEntry } from "~/lib/shares";
import { getShareById } from "~/models/share.server";
import { Sorters } from "~/utils";

export const loader: LoaderFunction = async ({ params }): Promise<Response> => {
  const shareFilePath = params["*"] || "";
  const shareId = params["shareId"];

  if (!shareId) {
    return redirect("/shares");
  }

  const share = await getShareById(shareId);

  if (!share) {
    return redirect("/shares");
  }

  const relativePathToOpen = path.join(share.path, shareFilePath);

  const dirEntry = await readDirectoryEntry({ relativePathToOpen });

  const shareableDirEntry = await createShareableDirectoryEntry(
    share,
    dirEntry
  );

  shareableDirEntry.children?.sort(Sorters.byDirectory);

  return json<ShareableDirectoryEntry>(shareableDirEntry);
};

export default function Index() {
  const dirEntry = useLoaderData<ShareableDirectoryEntry>();

  return (
    <div>
      <FilePathHeader>
        <Breadcrumbs pathStr={dirEntry.pathRelativeToShare} />
      </FilePathHeader>
      <SharesTable dirEntry={dirEntry} />
    </div>
  );
}
