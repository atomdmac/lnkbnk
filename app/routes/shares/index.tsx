import H1 from "~/components/H1";

export default function () {
  return (
    <div className="container flex flex-col items-center pt-4">
      <H1>Yeah...</H1>
      <p>That's not a thing.</p>
    </div>
  );
}
