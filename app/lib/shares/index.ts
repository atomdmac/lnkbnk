import type { Share } from "@prisma/client";
import type { DirectoryEntry, FullDirectoryEntry } from "~/lib/file-io";
import {
  isFile,
  getAbsolutePath,
  getDirNameRelativeToShare,
} from "~/lib/file-io";
import type {
  ShareableDirectoryEntry,
  ShareableDirectoryEntrySummary,
} from "./types.d";
export * from "./types.d";

/**
 * Convert directory entries intended for admins and server-side functionality
 * (i.e. FullDirectoryEntry objects) to a format suitable for consumption by
 * normal users.
 */
export async function createShareableDirectoryEntry(
  share: Share,
  fullDirectoryEntry: FullDirectoryEntry
): Promise<ShareableDirectoryEntry> {
  const pathRelativeToShare = getDirNameRelativeToShare(
    share,
    getAbsolutePath(fullDirectoryEntry.dirNameRelativeToRoot)
  );

  if (await shareIsSingleFile(share)) {
    const publicUrl = [share.id, pathRelativeToShare]
      .filter((s) => !!s)
      .join("/");

    const shareableFile: ShareableDirectoryEntry = {
      shareId: share.id,
      baseName: fullDirectoryEntry.baseName,
      isDirectory: false,
      children: null,
      pathRelativeToShare,
      pathToBrowse: `/shares/${publicUrl}`,
      pathToDownload: `/download/${publicUrl}`,
    };

    // Create a "faux share" that wraps the share that is being requested. This
    // has the effect of returning a "fake directory" that contains _only_ the
    // file referred to by the share.  In this way, a single file can be shared
    // and viewed without needing to grant access to the entire contents of the
    // shared file's parent directory.
    const shareableParent: ShareableDirectoryEntry = {
      shareId: share.id,
      baseName: fullDirectoryEntry.baseName,
      isDirectory: true,
      children: [shareableFile],
      pathRelativeToShare,
      pathToBrowse: `/shares/${publicUrl}`,
      pathToDownload: `/download/${publicUrl}`,
    };
    return shareableParent;
  }

  const children = !fullDirectoryEntry.children
    ? null
    : await Promise.all(
        fullDirectoryEntry.children.map(
          async (child) => await createShareableDirectoryEntry(share, child)
        )
      );

  const publicUrl = [share.id, pathRelativeToShare, fullDirectoryEntry.baseName]
    .filter((s) => !!s)
    .join("/");

  const shareableDirEntry: ShareableDirectoryEntry = {
    shareId: share.id,
    baseName: fullDirectoryEntry.baseName,
    isDirectory: fullDirectoryEntry.isDirectory,
    children,
    pathRelativeToShare,
    pathToBrowse: `/shares/${publicUrl}`,
    pathToDownload: `/download/${publicUrl}`,
  };

  return shareableDirEntry;
}

export function createShareableDirectoryEntrySummary(summary: {
  path: string;
  _count: { id: number };
}): ShareableDirectoryEntrySummary {
  const { path, _count } = summary;
  return {
    pathRelativeToRoot: path,
    // TODO: Is there a way to create a pathToBrowse that doesn't make assumpptions about the current location?
    pathToBrowse: `../shares/${path}`,
    count: _count.id,
  };
}

export function createPublicPath({
  share,
  directoryEntry,
}: {
  share: Share;
  directoryEntry: DirectoryEntry;
}) {
  const pathRelativeToShare = getDirNameRelativeToShare(
    share,
    getAbsolutePath(directoryEntry.baseName)
  );

  return [share.id, pathRelativeToShare, directoryEntry.baseName]
    .filter((s) => !!s)
    .join("/");
}

export async function shareIsSingleFile(share: Share): Promise<boolean> {
  return await isFile(share.path);
}
