import type { DirectoryEntry } from "~/lib/file-io";

export type ShareableDirectoryEntry = Omit<DirectoryEntry, "children"> & {
  shareId: string;
  pathRelativeToShare: string;
  pathToBrowse: string;
  pathToDownload: string;
  children: ShareableDirectoryEntry[] | null;
};

export type ShareableDirectoryEntrySummary = {
  pathRelativeToRoot: string;
  pathToBrowse: string;
  count: number;
};
