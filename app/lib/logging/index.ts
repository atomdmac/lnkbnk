import type { Logger } from "pino";
import { pino } from "pino";

const {
  LOG_LEVEL = "debug",
  LOG_DIRECTORY = "./",
  LOKI_URL,
  LOKI_USERNAME,
  LOKI_PASSWORD,
  NODE_ENV,
} = process.env;

declare module global {
  let logger: Logger;
}

if (!global.logger) {
  const targets = [];
  if (NODE_ENV === "off__production") {
    targets.push({
      level: LOG_LEVEL,
      target: "pino-loki",
      options: {
        host: LOKI_URL,
        batching: true,
        interval: 1,
        timeout: 5000,
        labels: {
          environment: process.env.NODE_ENV,
        },
        basicAuth: {
          username: LOKI_USERNAME,
          password: LOKI_PASSWORD,
        },
      },
    });
  } else {
    targets.push(
      {
        level: LOG_LEVEL,
        target: "pino/file",
        options: {
          destination: `${LOG_DIRECTORY}/lnkbnk.log`,
          append: true,
        },
      },
      {
        level: LOG_LEVEL,
        target: "pino-pretty",
        options: { destination: 1 },
      }
    );
  }

  global.logger = pino({
    level: LOG_LEVEL,
    transport: {
      targets,
    },
  });
}

export default global.logger;
