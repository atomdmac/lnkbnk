import { transport } from "pino";

export default transport({
  target: "pino-pretty",
  options: { destination: 1 },
});
