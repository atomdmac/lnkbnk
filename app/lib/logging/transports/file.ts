import { transport } from "pino";

console.log("LOGGING TO ", process.env.LOG_DIRECTORY);
export default transport({
  target: "pino/file",
  options: { destination: process.env.LOG_DIRECTORY, append: false },
});
