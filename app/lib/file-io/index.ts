import type { Share } from "@prisma/client";
import fs from "fs/promises";
import path from "path";
import type { FullDirectoryEntry } from "./types";
export * from "./types.d";

/**
 * Get the absolute path on disk given a relative share path.
 */
export const getAbsolutePath = (relativeSharePath: string = "") =>
  path.join(path.resolve(process.env.SHARE_ROOT || "."), relativeSharePath);

/**
 * Given a path relative to the root share path, determine whether or not the
 * corresponding directory entry is a file or not.
 */
export const isFile = async (relativeSharePath: string): Promise<boolean> => {
  try {
    const targetPath = getAbsolutePath(relativeSharePath);
    return (await fs.stat(targetPath)).isFile();
  } catch (error) {
    return false;
  }
};

/**
 * Get the directory (without the file name) relative to the Share's base path.
 */
export const getDirNameRelativeToShare = (
  share: Share,
  absoluteFilePath: string
) => path.relative(getAbsolutePath(share.path), absoluteFilePath);

/**
 * Get the directory (without the file name) relative to the shareable
 * filesystem root.
 */
export const getDirNameRelativeToRoot = (absoluteFilePath: string) =>
  path.dirname(path.relative(getAbsolutePath(), absoluteFilePath));

export const getPathRelativeToRoot = (absoluteDirEntryPath: string) =>
  path.relative(getAbsolutePath(), absoluteDirEntryPath);

/**
 * Return a DirectoryEntry object and it's immediate children.
 */
export const readDirectoryEntry = async ({
  relativePathToOpen,
}: {
  relativePathToOpen: string;
}): Promise<FullDirectoryEntry> => {
  const children: FullDirectoryEntry[] = [];
  const topLevelAbsolutePath = getAbsolutePath(relativePathToOpen);

  // If the DirectoryEntry is a file...
  if (await isFile(relativePathToOpen)) {
    const file: FullDirectoryEntry = {
      baseName: path.basename(relativePathToOpen),
      dirNameRelativeToRoot: relativePathToOpen,
      pathRelativeToRoot: relativePathToOpen,
      isDirectory: false,
      children: null,
    };
    return file;
  }

  // ...else, the DirectoryEntry must be a directory
  const dir = await fs.opendir(topLevelAbsolutePath);

  while (true) {
    const dirEnt = await dir.read();
    if (dirEnt === null) {
      break;
    }

    const childAbsolutePath = `${topLevelAbsolutePath}/${dirEnt.name}`;

    children.push({
      baseName: dirEnt.name,
      dirNameRelativeToRoot: getDirNameRelativeToRoot(childAbsolutePath),
      pathRelativeToRoot: getPathRelativeToRoot(childAbsolutePath),
      isDirectory: dirEnt.isDirectory(),
      children: null,
    });
  }

  const parent: FullDirectoryEntry = {
    baseName: path.basename(relativePathToOpen),
    dirNameRelativeToRoot: relativePathToOpen,
    pathRelativeToRoot: relativePathToOpen,
    isDirectory: true,
    children,
  };

  await dir.close();

  return parent;
};
