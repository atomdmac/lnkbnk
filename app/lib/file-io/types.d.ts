export type DirectoryEntry = {
  // The file/directory name without the path.
  baseName: string;
  // Whether the directory entry is a directory or not.
  isDirectory: boolean;
  // If a directory, a list of child directory entries, else null.
  children: FullDirectoryEntry[] | null;
};

export type FullDirectoryEntry = DirectoryEntry & {
  // The dirname to the directory entry relative to the root share directory.
  // If a file, this does not include the file name.
  dirNameRelativeToRoot: string;

  // Path to the directory entry relative to the root share directory.
  // If a file, this *does* include the file name.
  pathRelativeToRoot: string;
};
