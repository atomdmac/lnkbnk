import type { User } from "@prisma/client";

import { prisma } from "~/db.server";

export type { User } from "@prisma/client";

export async function createSyncthingDeviceId(
  userId: User["id"],
  deviceId: string
) {
  return prisma.syncthingDeviceId.create({
    data: {
      userId,
      deviceId,
    },
  });
}
