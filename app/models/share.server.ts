import { prisma } from "~/db.server";

export type { Share } from "@prisma/client";

const getExpiration = () => {
  // One week
  return new Date(Date.now() + 1000 * 60 * 60 * 24 * 7);
};

export async function createShare({ path }: { path: string }) {
  return await prisma.share.create({
    data: {
      path,
      expiresAt: getExpiration(),
    },
  });
}

export async function getShares(path: string | undefined = undefined) {
  if (!path) {
    return prisma.share.findMany();
  }
  return prisma.share.findMany({
    where: {
      path,
    },
  });
}

export async function getAdminShares(path: string | undefined = undefined) {
  if (!path) {
    return await prisma.share.groupBy({
      by: ["path"],
      _count: {
        id: true,
      },
    });
  }

  return await prisma.share.groupBy({
    by: ["path"],
    _count: {
      id: true,
    },
    where: {
      path,
    },
  });
}

export async function getShareById(shareId: string) {
  return prisma.share.findUnique({
    where: {
      id: shareId,
    },
  });
}

export async function deleteShare(id: string) {
  return prisma.share.delete({
    where: {
      id,
    },
  });
}

export async function getShareCount(path: string): Promise<number> {
  return await prisma.share.count({
    where: {
      path,
    },
  });
}
