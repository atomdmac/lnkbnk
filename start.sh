#!/bin/sh

set -ex

# Ensure the database is up to date with the schema
npx prisma db push

# Before starting the server, we need to run any prisma migrations that haven't 
# yet been run, which is why this file exists in the first place.
# Learn more: https://community.fly.io/t/sqlite-not-getting-setup-properly/4386
npx prisma migrate deploy

# Run the app
npm run start
